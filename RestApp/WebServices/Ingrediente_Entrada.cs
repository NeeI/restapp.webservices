//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebServices
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ingrediente_Entrada
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ingrediente_Entrada()
        {
            this.Plato_Entrada = new HashSet<Plato_Entrada>();
        }
    
        public int ingrediente_entrada_id { get; set; }
        public string ingrediente_entrada_nombre { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Plato_Entrada> Plato_Entrada { get; set; }
    }
}
