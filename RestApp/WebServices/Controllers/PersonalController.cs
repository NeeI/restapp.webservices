﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class PersonalController : ApiController
    {

        DBRestAppEntities db = new DBRestAppEntities();


        //[GET] Obtener Datos Personales desde el numero de celular
        public DatoPersonalModels GetDatoPersonal(string celular)
        {
            var query = db.sp_obtenerUsuario(celular).FirstOrDefault();


            DatoPersonalModels datoPersonal = new DatoPersonalModels
            {
                celular = query.usuario_celular,
                acceso = query.tipo_acceso_id,
                nombre = query.personal_nombre,
                apellido = query.personal_apellido,
                correo = query.personal_correo,
                cumpleanio = query.persona_cumpleanio

            };
            return datoPersonal;


        }
    }
}
