﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class UsuarioController : ApiController
    {
        DBRestAppEntities db = new DBRestAppEntities();

        //[GET] logear y validar el acceso a la cuenta 

        //Result == 0, entonces no existe
        //Result == -1, Esta bloqueada
        //Result == 1, Ingresa a su cuenta
        public ResultMessage GetUsuario(string celular)
        {
            string msg = "";

            ResultMessage resultMessage = new ResultMessage();

            var resp = db.sp_login(celular);
            string result = resp.SingleOrDefault().ToString();

            //Mapeamso a nuestro Modelo ResultMessage
            resultMessage.response = result;

            return resultMessage;

        }


    }
}
